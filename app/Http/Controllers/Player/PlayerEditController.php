<?php

namespace App\Http\Controllers\Player;

use App\Infrastructure\Services\PlayerService;
use Illuminate\Contracts\View\View;

class PlayerEditController
{
    public function __construct(
        protected PlayerService $playerService
    ){}

    public function __invoke(int $id): View
    {
        $player = $this->playerService->getById($id);
        return view('players.edit', compact('player'));
    }
}
