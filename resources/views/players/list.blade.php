<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Players') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="w-full flex items-center justify-center mb-4">
                <div class="rounded border w-48">
                    <h1 class="py-4 text-center text-white">Goalkeepers</h1>
                    <h2 class="text-center text-white">{{ count($goalKeepers) }}</h2>
                </div>
                <div class="rounded border w-48 ml-3">
                    <h1 class="py-4 text-center text-white">Common Players</h1>
                    <h2 class="text-center text-white">{{ count($commonPlayers) }}</h2>
                </div>
                <div class="rounded border w-48 ml-3">
                    <h1 class="py-4 text-center text-white">Not Present</h1>
                    <h2 class="text-center text-white">{{ count($notPresent) }}</h2>
                </div>
            </div>
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                   <div class="w-full flex justify-end">
                       <form action="{{ route('players-per-team.update') }}" method="post" class="inline-flex" id="formUpdatePlayersPerTeam">
                           @csrf
                           <x-input-label class="mt-2 mr-2">Players Per Team</x-input-label>
                           <select name="players_per_team" id="players_per_team" class="px-1 mr-2 w-48 h-9 dark:bg-gray-800 text-white">
                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="4">4</option>
                               <option value="5">5</option>
                               <option value="6">6</option>
                           </select>
                       </form>
                       <a href="{{ route('players.draw') }}" class="border-transparent py-2 rounded-md mr-2 px-2 mb-4 dark:bg-indigo-900/50 dark:text-gray-400 dark:text-white">Draw</a>
                       <a href="{{ route('players.new') }}" class="border-transparent py-2 rounded-md px-2 mb-4 dark:bg-indigo-900/50 dark:text-gray-400 dark:text-white">New</a>
                   </div>
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead class="text-sm text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Goalkeeper</th>
                                <th>Present</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($playerPaginated as $player)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td class="px-6 py-4">{{ $player->name }}</td>
                                    <td class="px-6 py-4">{{ $player->level }}</td>
                                    <td class="px-6 py-4">{{ $player->goalkeeper ? 'Sim' : 'Não' }}</td>
                                    <td class="px-6 py-4">{{ $player->present ? 'Sim' : 'Não' }}</td>
                                    <td>
                                        <a href="{{ route('player.edit', ['id' => $player->id]) }}">Edit</a>
                                        <form action="{{ route('player.delete', ['id' => $player->id]) }}" method="post" class="inline-flex">
                                            @csrf
                                            @method('delete')
                                            <button type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $playerPaginated->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
