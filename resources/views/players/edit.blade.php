<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Player Edit') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    @if (session()->has('message'))
                        <div class="w-full text-center">
                            <span class="{{ session()->get('success') ? 'text-green-600' : 'text-red-600' }}">
                                {{ session()->get('message') }}
                            </span>
                        </div>
                    @endif
                    <form action="{{ route('player.update', ['id' => $player->id]) }}" method="post">
                        @csrf
                        @method('put')
                        <x-input-label for="name">Name: </x-input-label>
                        <x-text-input id="name" class="w-full" :value="$player->name" name="name"></x-text-input>
                        <x-input-label for="level">Level: </x-input-label>
                        <x-text-input type="number" min="1" max="5" id="level" class="w-full" :value="$player->level" name="level"></x-text-input>
                        <x-input-label for="goalkeeper">Goalkeeper: </x-input-label>
                        <select name="goalkeeper" id="goalkeeper" class="w-full border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm">
                            <option value="1" {{ $player->goalkeeper == '1' ? 'selected' : '' }}>Sim</option>
                            <option value="0"  {{ $player->goalkeeper == '0' ? 'selected' : '' }}>Não</option>
                        </select>
                        <x-input-label for="present">Present: </x-input-label>
                        <select name="present" id="present" class="w-full border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm">
                            <option value="1" {{ $player->present == '1' ? 'selected' : '' }}>Sim</option>
                            <option value="0"  {{ $player->present == '0' ? 'selected' : '' }}>Não</option>
                        </select>
                        <button type="submit" class="rounded py-2 px-2 mt-2 border-indigo-400 dark:bg-indigo-900/50">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
