<?php

namespace App\Http\Controllers\Player;

use App\Infrastructure\Services\PlayerService;
use Illuminate\Contracts\View\View;

class PlayerNewController
{
    public function __construct(
        protected PlayerService $playerService
    ){}

    public function __invoke(): View
    {
        return view('players.new');
    }
}
