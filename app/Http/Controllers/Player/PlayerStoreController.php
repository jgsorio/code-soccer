<?php

namespace App\Http\Controllers\Player;

use App\Http\Requests\PlayerStoreRequest;
use App\Infrastructure\Services\PlayerService;

class PlayerStoreController
{
    public function __construct(
        protected PlayerService  $playerService
    ){}
    public function __invoke(PlayerStoreRequest $request)
    {
        $player = $this->playerService->store($request->validated());
        if (!$player) {
            return redirect()->back()->with(['success' => false, 'message' => 'Ocorreu um erro ao tentar salvar os dados.']);
        }
        return redirect()->back()->with(['success' => true, 'message' => 'Joagador incluído com sucesso!']);
    }
}
