<?php

namespace App\Domain\Repositories;

use App\Models\Player;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface PlayerRepositoryInterface
{
    public function all(array $filters = []): Collection;
    public function getById(int $id): ?Player;
    public function update(array $data, int $id): bool;
    public function delete(int $id): void;
    public function paginate(): Paginator;
    public function store(array $data): Player;
}
