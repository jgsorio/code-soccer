<?php

namespace App\Domain\Repositories;

use App\Models\Draw;

interface DrawRepositoryInterface
{
    public function store(string $data): bool;
    public function getLatest(): ?Draw;
}
