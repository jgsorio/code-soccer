<?php

namespace App\Infrastructure\Services;
ini_set('max_execution_time', '300');

use App\Domain\Repositories\PlayerRepositoryInterface;
use App\Models\Player;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class PlayerService
{
    public function __construct(
        protected PlayerRepositoryInterface $playerRepository,
        protected DrawService $drawService,
    ){}

    public function all(): Collection
    {
        return $this->playerRepository->all();
    }

    public function draw(): array
    {
        return $this->drawService->draw();
    }

    public function getById(int $id): ?Player
    {
        return $this->playerRepository->getById($id);
    }

    public function update(array $data, int $id): bool
    {
        return $this->playerRepository->update($data, $id);
    }

    public function delete(int $id): void
    {
        $this->playerRepository->delete($id);
    }

    public function paginate(): Paginator
    {
        return $this->playerRepository->paginate();
    }

    public function store(array $data): Player
    {
        return $this->playerRepository->store($data);
    }
}
