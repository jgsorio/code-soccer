<?php

namespace App\Infrastructure\Services;

use App\Domain\Repositories\DrawRepositoryInterface;
use App\Domain\Repositories\PlayerRepositoryInterface;
use App\Models\Draw;
use Illuminate\Support\Facades\Redis;

class DrawService
{
    public function __construct(
        protected PlayerRepositoryInterface $playerRepository,
        protected DrawRepositoryInterface $drawRepository,
    ){}
    public function draw(): array
    {
        $teams = [];
        $players = $this->playerRepository->all(['present' => 1])->toArray();
        $goalkeepers = array_filter($players, function ($player) {
            return $player['goalkeeper'] == 1;
        });
        $commonPlayers = array_filter($players, function ($player) {
            return $player['goalkeeper'] == 0;
        });

        if (count($goalkeepers) < 2) {
            return [
                'message' => 'Goleiros insuficientes',
                'success' => false,
                'teams' => $teams
            ];
        }

        if (count($commonPlayers) < env('PLAYERS_PER_TEAM', 6) * 2 - 2) {
            return [
                'message' => 'Os times estão incompletos',
                'success' => false,
                'teams' => $teams
            ];
        }

        while (count($commonPlayers) && count($goalkeepers)) {
            $team = [];
            $levelTeam = 0;
            $goalkeeper = array_rand($goalkeepers);
            $team['goalkeeper'] = $goalkeepers[$goalkeeper]['name'];
            $levelTeam += $goalkeepers[$goalkeeper]['level'];
            unset($goalkeepers[$goalkeeper]);

            while (count($team) < (int)env('PLAYERS_PER_TEAM', 6)) {
                if (empty($commonPlayers)) {
                    $team[] = null;
                    continue;
                }
                $playerDrawn = array_rand($commonPlayers);
                if ($commonPlayers[$playerDrawn]['level'] + $levelTeam < 30) {
                    $team['player_' . count($team)] = $commonPlayers[$playerDrawn]['name'];
                    $levelTeam += $commonPlayers[$playerDrawn]['level'];
                    unset($commonPlayers[$playerDrawn]);
                }
            }
            $team['level'] = $levelTeam;
            $teams[] = $team;
        }

        $this->drawRepository->store(json_encode($teams, JSON_UNESCAPED_UNICODE));
        $redis = Redis::connection();
        $redis->set('teams', json_encode($teams), 'EX', 60);

        return ['success' => true, 'message' => 'Sorteio realizado com sucesso', 'teams' => $teams];
    }

    public function getLatest(): ?Draw
    {
        return $this->drawRepository->getLatest();
    }
}
