<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Repositories\PlayerRepositoryInterface;
use App\Models\Player;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class PlayerRepository implements PlayerRepositoryInterface
{
    public function __construct(
        protected Player $model
    ){}
    public function all(array $filters = []): Collection
    {
        return $this->model->where(function ($query) use ($filters) {
            foreach ($filters as $field => $value) {
                $query->where($field, '=', $value);
            }
        })->get();
    }

    public function getById(int $id): ?Player
    {
        return $this->model->findOrFail($id);
    }

    public function update(array $data, int $id): bool
    {
        $player = $this->model->findOrFail($id);
        return $player->update($data);
    }

    public function delete(int $id): void
    {
        $player = $this->model->findOrFail($id);
        $player->delete();
    }

    public function paginate(): Paginator
    {
        return $this->model->paginate();
    }

    public function store(array $data): Player
    {
        return $this->model->create($data);
    }
}
