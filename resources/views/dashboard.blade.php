<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="w-full flex justify-end">
                <a href="{{ route('draw.latest') }}" class="py-2 px-2 mb-4 rounded border-transparent dark:bg-indigo-900/50 text-white">Latest Draw</a>
            </div>
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    @if(session()->has('message'))
                        <div class="w-full py-2 text-center">
                            <span class="{{ session()->get('success') ? 'text-green-600' : 'text-red-600' }}">
                                {{ session()->get('message') }}
                            </span>
                        </div>
                    @endif
                    <div class="w-full flex justify-between">
                        @forelse($teams as $key => $team)
                            <div class="w-full bg-white text-black px-2 ml-3 rounded">
                                <h1 class="mb-4 py-2 text-gray-900 text-center text-lg font-semibold">Time: {{ $key }} - Level: {{ $team['level'] }}</h1>
                                <ul class="w-full py-2">
                                    <li class="py-2 border-b">Goleiro - {{ $team['goalkeeper'] ?? 'Vazio' }}</li>
                                    @if (env('PLAYERS_PER_TEAM') >= 2)
                                        <li class="py-2 border-b" >Zagueiro - {{ $team['player_1'] ?? 'Vazio' }}</li>
                                    @endif
                                    @if (env('PLAYERS_PER_TEAM') >= 3)
                                        <li class="py-2 border-b">Lateral - {{ $team['player_2'] ?? 'Vazio' }}</li>
                                    @endif
                                    @if (env('PLAYERS_PER_TEAM') >= 4)
                                        <li class="py-2 border-b">Meia - {{ $team['player_3'] ?? 'Vazio' }}</li>
                                    @endif
                                    @if (env('PLAYERS_PER_TEAM') >= 5)
                                        <li class="py-2 border-b">Centro Avante - {{ $team['player_4'] ?? 'Vazio' }}</li>
                                    @endif
                                    @if (env('PLAYERS_PER_TEAM') >= 6)
                                        <li class="py-2 border-b">Atacante - {{ $team['player_5'] ?? 'Vazio' }}</li>
                                    @endif
                                </ul>
                            </div>
                        @empty
                            <h1 class="w-full text-center">Nenhum sorteio realizado</h1>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
