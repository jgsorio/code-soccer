<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Repositories\DrawRepositoryInterface;
use App\Models\Draw;

class DrawRepository implements DrawRepositoryInterface
{
    public function __construct(
        protected Draw $model
    ){}
    public function store(string $data): bool
    {
        $this->model->data = $data;
        return $this->model->save();
    }

    public function getLatest(): ?Draw
    {
        return $this->model->latest()->first();
    }
}
