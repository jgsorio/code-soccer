<?php

namespace App\Http\Controllers\Draw;

use App\Infrastructure\Services\DrawService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redis;

class DrawLatestController
{
    public function __construct(
        protected DrawService $drawService
    ){}

    public function __invoke(): RedirectResponse
    {
        $latestDraw = $this->drawService->getLatest();
        if (empty($latestDraw)) {
            return redirect()->back()->with(['success' => false, 'message' => 'Nenhum registro encontrado.']);
        }
        $redis = Redis::connection();
        $redis->set('teams', $latestDraw->data, 'EX', 60);

        return redirect()->back()->with([
            'success' => true,
            'message' => 'Ultimo registro encontrado. Data: ' . date('d/m/Y', strtotime($latestDraw->created_at)),
            'teams' => json_decode($latestDraw->data, true)
        ]);
    }
}
