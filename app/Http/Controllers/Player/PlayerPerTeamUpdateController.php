<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;

class PlayerPerTeamUpdateController
{
    public function __invoke(Request $request)
    {
        $data = (int)$request->get('players_per_team');
        $envFile = base_path('.env');
        $contents = file_get_contents($envFile);

        $lines = explode("\n", $contents);
        foreach ($lines as &$line) {
            if (str_contains($line, 'PLAYERS_PER_TEAM')) {
                $line = str_replace($line, "PLAYERS_PER_TEAM={$data}", $line);
            }
        }

        file_put_contents($envFile, implode("\n", $lines));

        return redirect()->back();
    }
}
