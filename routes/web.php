<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Draw\DrawLatestController;
use App\Http\Controllers\Player\{
    PlayerDeleteController,
    PlayerDrawController,
    PlayerEditController,
    PlayerListAllController,
    PlayerNewController,
    PlayerPerTeamUpdateController,
    PlayerStoreController,
    PlayerUpdateController
};

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::get('/dashboard', DashboardController::class)->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/players', PlayerListAllController::class)->name('players.list');
    Route::get('/players/new', PlayerNewController::class)->name('players.new');
    Route::post('/players/store', PlayerStoreController::class)->name('players.store');
    Route::get('/players/draw', PlayerDrawController::class)->name('players.draw');
    Route::get('/players/{id}', PlayerEditController::class)->name('player.edit')->where('id', '[0-9]+');
    Route::put('/players/{id}', PlayerUpdateController::class)->name('player.update')->where('id', '[0-9]+');
    Route::delete('/player/{id}', PlayerDeleteController::class)->name('player.delete')->where('id', '[0-9]+');

    Route::get('/draw/latest', DrawLatestController::class)->name('draw.latest');
    Route::post('/players-per-team', PlayerPerTeamUpdateController::class)->name('players-per-team.update');
});

require __DIR__.'/auth.php';
