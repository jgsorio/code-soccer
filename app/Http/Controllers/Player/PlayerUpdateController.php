<?php

namespace App\Http\Controllers\Player;

use App\Infrastructure\Services\PlayerService;
use Illuminate\Http\Request;

class PlayerUpdateController
{
    public function __construct(
        protected PlayerService $playerService
    ){}

    public function __invoke(Request $request, int $id)
    {
       try {
           $data = $request->only(['name', 'level', 'goalkeeper', 'present']);
           $this->playerService->update($data, $id);
           return redirect()->back()->with(['success' => true, 'message' => 'Player atualizado com sucesso!']);
       } catch (\Exception $e) {
           return redirect()->back()->with(['success' => false, 'message' => 'Ocorreu um erro ao atualizar o player']);
       }
    }
}
