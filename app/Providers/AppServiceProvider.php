<?php

namespace App\Providers;

use App\Domain\Repositories\DrawRepositoryInterface;
use App\Domain\Repositories\PlayerRepositoryInterface;
use App\Infrastructure\Repositories\DrawRepository;
use App\Infrastructure\Repositories\PlayerRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(PlayerRepositoryInterface::class, PlayerRepository::class);
        $this->app->bind(DrawRepositoryInterface::class, DrawRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
