<?php

namespace App\Http\Controllers\Player;

use App\Infrastructure\Services\PlayerService;
use Illuminate\Contracts\View\View;

class PlayerListAllController
{
    public function __construct(
        protected PlayerService $playerService
    ){}

    public function __invoke(): View
    {
        $players = $this->playerService->all();
        $commonPlayers = array_filter($players->toArray(), function ($player) {
           return $player['goalkeeper'] == 0 && $player['present'] == 1;
        });

        $notPresent = array_filter($players->toArray(), function ($player) {
            return $player['present'] == 0;
        });

        $goalKeepers = array_filter($players->toArray(), function ($player) {
            return $player['goalkeeper'] == 1 && $player['present'] == 1;
        });

        $playerPaginated = $this->playerService->paginate();

        return view('players.list', compact('players', 'commonPlayers', 'goalKeepers', 'notPresent', 'playerPaginated'));
    }
}
