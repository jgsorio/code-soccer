# Projeto Code Soccer

## Instalação
Para instalar tenha o [composer]("https://getcomposer.org/") instalado na sua máquina e também o [php]("https://www.php.net/") na versão 8.2 <br>
Utilizei nesse projeto o Laravel Sail, mas caso não possua o [docker]("https://www.docker.com/") instalado. <br> Renomeie o arquivo .env.example para .env
e troque as credenciais do banco de dados para o seu banco de dados, no meu caso utilizei o [mysql]("https://www.mysql.com/") e troque também as credenciais do [redis]("https://redis.io/").
<br>

```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=sail
DB_PASSWORD=password

REDIS_CLIENT=phpredis
REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379
```

## Instalar as dependencias
```
composer install
```

## Rodar o projeto Via Docker
Para rodar o projeto via docker digite o comando abaixo:

```
./vendor/bin/sail up -d
```

## Rodar o projeto local
```
php artisan serve
```

## Gerar  chave do projeto via Docker
```
./vendor/bin/sail artisan key:generate
```

## Gerar  chave do projeto local
```
php artisan key:generate
```

## Banco de Dados
Para realizar a criação e a inclusão dos dados realize os seguintes procedimentos:

### Local
```
php artisan migrate --seed
```

### Docker
```
./vendor/bin/sail artisan migrate --seed
```

## Acesso
Para o acesso ao sistema acesse http://localhost

Usuario: admin@admin.com
Senha: password


## Lógica usada para efetuar o nivelamento dos times
Utilizei uma variavel que recebe 0 inicialmente e que vai incrementando a cada jogador sorteado somando o nivel do mesmo.
Caso o jogador sorteado + o valor do nivel ultrapasse o maximo permitido, havera novamente o sorteio de outro jogador, até que
os times estejam completos.



### Links
docker -> https://www.docker.com/ <br>
php -> https://www.php.net/ <br>
mysql -> https://www.mysql.com/ <br>
redis -> https://redis.io/ <br>
