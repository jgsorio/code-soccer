<?php

namespace App\Http\Controllers\Player;

use App\Infrastructure\Services\PlayerService;
use Illuminate\Http\RedirectResponse;

class PlayerDeleteController
{
    public function __construct(
        protected PlayerService $playerService
    ){}

    public function __invoke(int $id): RedirectResponse
    {
        $this->playerService->delete($id);
        return redirect()->back();
    }
}
