<?php

namespace App\Http\Controllers;

use App\Infrastructure\Services\DrawService;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Redis;

class DashboardController
{
    public function __construct(
        protected DrawService $drawService
    ){}

    public function __invoke(): View
    {
        $redis = Redis::connection();
        $teams = !empty($redis->get('teams')) ? json_decode($redis->get('teams'), true) : [];
        return view('dashboard', compact('teams'));
    }
}
