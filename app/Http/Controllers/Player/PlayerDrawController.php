<?php

namespace App\Http\Controllers\Player;

use App\Http\Controllers\Controller;
use App\Infrastructure\Services\PlayerService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PlayerDrawController extends Controller
{
    public function __construct(
        protected PlayerService  $playerService
    ){}

    public function __invoke(): RedirectResponse
    {
        $response = $this->playerService->draw();
        return redirect()->route('dashboard')->with($response);
    }
}
